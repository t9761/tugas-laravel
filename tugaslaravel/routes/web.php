<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@utama');
Route::get('/register', 'AuthController@bio');
Route::get('/welcome', 'AuthController@welcome');

route::get('/table', function(){
    return view('page.table');
});

route::get('/data-table', function(){
    return view('page.data-table');
});

//CRUD CAST

Route::get('/cast/create', 'CastController@create');

